package br.com.dbc.poatransporteback.Controller;

import br.com.dbc.poatransporteback.Entity.LinhaOnibus;
import br.com.dbc.poatransporteback.Service.LinhaOnibusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/linha-onibus")
public class LinhaOnibusController {

    @Autowired
    private LinhaOnibusService linhaOnibusService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<LinhaOnibus> lstLinhasOnibus() {
        return linhaOnibusService.all();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public List<LinhaOnibus> novoOnibus( @RequestBody List<LinhaOnibus> onibus ) {
        return linhaOnibusService.salvar(onibus);
    }

    @GetMapping( value = "/nome={nome}" )
    @ResponseBody
    public LinhaOnibus buscarPorNome( @PathVariable String nome ) {
        return linhaOnibusService.buscarPorNome( nome );
    }

}
