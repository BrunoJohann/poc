package br.com.dbc.poatransporteback;

import br.com.dbc.poatransporteback.Entity.LinhaOnibus;
import br.com.dbc.poatransporteback.Util.FiltrosPesquisaUtil;
import com.google.gson.JsonArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Spike {

    public static void main(String[] args) throws IOException {

        LinhaOnibus onibus = new LinhaOnibus();
        onibus.setId(5530);
        LinhaOnibus onibus2 = new LinhaOnibus();
        onibus2.setId(5115);
        List<LinhaOnibus> listaOnibus = new ArrayList<>();
        listaOnibus.add(onibus);
        listaOnibus.add(onibus2);

        URL url = new URL("http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o");
        HttpURLConnection httpUrl = (HttpURLConnection) url.openConnection();

        BufferedReader br = new BufferedReader( new InputStreamReader( httpUrl.getInputStream() ));
        String resultado = br.readLine();

        List<String> resultadoSplit = Stream.of(resultado.split("}, "))
                .collect(Collectors.toList());

        String resultadoSplitString = Stream.of(resultado.split("},"))
                .filter( res -> Pattern.compile("\"id\":\"5530\"").matcher(res).find() )
                .map( res -> res+="}")
                .findFirst()
                .orElse("Não há resultado para busca");
//        resultadoSplit.forEach( System.out::println );
//        System.out.println( resultado );
//        System.out.println( resultadoSplitString );

//        List<String> splitRes = FiltrosPesquisaUtil.splitResultadoApiTeste( resultado, 5530, 5529 );
        List<LinhaOnibus> testeComparador = FiltrosPesquisaUtil.comparador( resultado, listaOnibus );
        System.out.println( testeComparador );

        System.out.println(FiltrosPesquisaUtil.bancoDeveAtualizar(listaOnibus, testeComparador ));

    }

}
