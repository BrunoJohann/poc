package br.com.dbc.poatransporteback.Service;

import br.com.dbc.poatransporteback.Entity.LinhaOnibus;
import br.com.dbc.poatransporteback.Repository.LinhaOnibusRepository;
import br.com.dbc.poatransporteback.Util.FiltrosPesquisaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Service
public class LinhaOnibusService {

    @Autowired
    private LinhaOnibusRepository linhaOnibusRepository;

    @Transactional(rollbackFor = Exception.class)
    public List<LinhaOnibus> salvar(List<LinhaOnibus> onibus) {
        List<LinhaOnibus> retorno = new ArrayList<>();
        for( LinhaOnibus bus : onibus ) {
            linhaOnibusRepository.save(bus);
            retorno.add(bus);
        }
        return verificacaoOnibus( retorno );
    }

    public List<LinhaOnibus> all() {
        return verificacaoOnibus( (List<LinhaOnibus>) linhaOnibusRepository.findAll() );
    }

    public LinhaOnibus buscarPorNome( String nome ) {
        LinhaOnibus retorno =  linhaOnibusRepository.findAllByNome( nome );
        return (LinhaOnibus) verificacaoOnibus((List<LinhaOnibus>) retorno);
    }

    private List<LinhaOnibus> verificacaoOnibus(List<LinhaOnibus> lista) {
        String end = FiltrosPesquisaUtil.enderecoApiOnibus();
        String resultadoApi = FiltrosPesquisaUtil.requisicaoApi( end );
        List<LinhaOnibus> lstOnibus = lista;
        List<LinhaOnibus> resultado = FiltrosPesquisaUtil.comparador( resultadoApi, lstOnibus );
        boolean resBoolean = FiltrosPesquisaUtil.bancoDeveAtualizar(  lstOnibus, resultado );
        if( resBoolean ) {
            for(LinhaOnibus linha : resultado) {
                linhaOnibusRepository.save(linha);
            }
        }
        return resultado;
    }



}
