package br.com.dbc.poatransporteback.Entity;

import javax.persistence.*;

@Entity
public class Itinerario {

    @Id
    private Integer id;

    private String nome;

    private String codigo;

    @OneToOne( mappedBy = "itinerario" )
    private LinhaOnibus linha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public LinhaOnibus getLinha() {
        return linha;
    }

    public void setLinha(LinhaOnibus linha) {
        this.linha = linha;
    }
}
