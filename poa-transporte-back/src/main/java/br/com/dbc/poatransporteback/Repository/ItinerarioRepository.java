package br.com.dbc.poatransporteback.Repository;

import br.com.dbc.poatransporteback.Entity.Itinerario;
import org.springframework.data.repository.CrudRepository;

public interface ItinerarioRepository extends CrudRepository<Itinerario, Integer > {

}
