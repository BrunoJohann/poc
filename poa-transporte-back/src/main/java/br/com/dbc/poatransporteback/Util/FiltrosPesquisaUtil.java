package br.com.dbc.poatransporteback.Util;

import br.com.dbc.poatransporteback.Entity.LinhaOnibus;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;

public class FiltrosPesquisaUtil {

    public static String enderecoApiOnibus() {
        return "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";
    }

    public static String requisicaoApi(String Url) {
        try{
            java.net.URL url = new URL(Url);
            HttpURLConnection httpUrl = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader( new InputStreamReader( httpUrl.getInputStream() ));
            return br.readLine();
        } catch ( Exception e ) {
            return e.getMessage();
        }
    }

    public static boolean bancoDeveAtualizar( List<LinhaOnibus> lstOnibus, List<LinhaOnibus> banco ) {
        String antes = new Gson().toJson(lstOnibus);
        String depois = new Gson().toJson(lstOnibus);
        if( antes.equals(depois) ) { return true; }
        else{ return false; }
    }

    public static List<LinhaOnibus> comparador( String resultadoApi, List<LinhaOnibus> lstOnibus ) {
        List<String> splitResApi = splitResultadoApiOnibus( resultadoApi, lstOnibus );
        List<String> toStringResBanco = buscarToStringDoBanco( lstOnibus );
        String onibusParaOBanco = conferirAtualizacao( splitResApi, toStringResBanco );
        return jsonParaObjeto( onibusParaOBanco );
    }

    public static String conferirAtualizacao( List<String> splitResApi, List<String> toStringResBanco ) {
        String resApi = "" + splitResApi + "";
        String resBanco = "" + toStringResBanco + "";
//        String resApi = new Gson().toJson( splitResApi );
//        String resBanco = new Gson().toJson( toStringResBanco );
        if( !resApi.equals(resBanco) ) {
            resBanco = resApi;
            return resBanco;
        }
        return resBanco;
    }

    public static List<LinhaOnibus> jsonParaObjeto( String linhas ) {
        return Stream.of( new Gson().fromJson( linhas, LinhaOnibus[].class ) )
                .collect(Collectors.toList());
    }

    public static String objetoToJson( List<LinhaOnibus> listaObjetoOnibus ) {
        return new Gson().toJson( listaObjetoOnibus );
    }

    public static List<String> buscarToStringDoBanco( List<LinhaOnibus> lista ) {
        return Stream.of(lista).map( res -> res.toString() )
                .collect(Collectors.toList());
    }

    public static List<String> splitResultadoApiOnibus( String resApi, List<LinhaOnibus> idsBanco ) {
        return Stream.of( new Gson().fromJson( resApi, LinhaOnibus[].class ) )
                .filter( res -> filtradorPorObjeto( res, idsBanco ) )
                .map( res -> res.toString() )
                .collect(Collectors.toList());
    }

    public static List<LinhaOnibus> buscaListaOnibusId(String stringListOnibus, List<Integer> idsPesquisa ) {
        return Stream.of( new Gson().fromJson( stringListOnibus, LinhaOnibus[].class ))
                .filter( res -> filtradorPorId( res, idsPesquisa ) )
                .collect(Collectors.toList());
    }


    public static Boolean filtradorPorId( LinhaOnibus linha, List<Integer> idsPesquisa ) {
        for( Integer i : idsPesquisa ) {
            if( linha.getId().equals(i) ){ return true; }
        }
        return false;
    }

    public static Boolean filtradorPorObjeto( LinhaOnibus linha, List<LinhaOnibus> idsPesquisa ) {
        Integer idLinha = linha.getId();
        for( LinhaOnibus linhaPesquisa : idsPesquisa ) {
            if( linhaPesquisa.getId().equals(idLinha) ){ return true; }
        }
        return false;
    }

    public static List<String> splitResultadoApiTeste( String resApi, Integer valor, Integer segundoValor ) {
        return Stream.of( new Gson().fromJson( resApi, LinhaOnibus[].class ) )
                .filter( res -> res.getId().equals(valor) || res.getId().equals(segundoValor) )
                .map( res -> res.toString() )
                .collect(Collectors.toList());
    }

}
