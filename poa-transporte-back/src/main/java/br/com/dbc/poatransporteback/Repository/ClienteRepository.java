package br.com.dbc.poatransporteback.Repository;

import br.com.dbc.poatransporteback.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}
