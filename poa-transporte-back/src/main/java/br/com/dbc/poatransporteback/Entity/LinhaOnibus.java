package br.com.dbc.poatransporteback.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class LinhaOnibus {

    @Id
    private Integer id;

    private String nome;

    private String codigo;

    @ManyToMany( mappedBy = "linhas")
    private List<Cliente> clientes;

    @OneToOne
    @JoinColumn( name = "ID_ITINERARIO" )
    private Itinerario itinerario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Itinerario getItinerario() {
        return itinerario;
    }

    public void setItinerario(Itinerario itinerario) {
        this.itinerario = itinerario;
    }

    @Override
    public String toString() {
        return "{\"id\":\"" + id + "\",\"codigo\":\"" + codigo + "\",\"nome\":\"" + nome + "\"}";
    }
}
