package br.com.dbc.poatransporteback.Repository;

import br.com.dbc.poatransporteback.Entity.LinhaOnibus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LinhaOnibusRepository extends CrudRepository<LinhaOnibus, Integer > {
    LinhaOnibus findAllByNome(String nome );
}
