package br.com.dbc.poatransporteback.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cliente {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String nome;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "CLIENTE_LINHA",
        joinColumns = {
            @JoinColumn( name = "ID_CLIENTE")},
    inverseJoinColumns = {
            @JoinColumn( name = "ID_LINHA")})
    private List<LinhaOnibus> linhas = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<LinhaOnibus> getLinhas() {
        return linhas;
    }

    public void setLinhas(List<LinhaOnibus> linhas) {
        this.linhas = linhas;
    }
}
