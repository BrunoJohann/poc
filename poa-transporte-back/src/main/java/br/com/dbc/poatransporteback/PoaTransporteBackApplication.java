package br.com.dbc.poatransporteback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoaTransporteBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoaTransporteBackApplication.class, args);
    }

}
