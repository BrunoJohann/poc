import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaLotacaoComponent } from './lista-lotacao/lista-lotacao.component'
import { ListaOnibusComponent } from './lista-onibus/lista-onibus.component'
import { ItinerarioComponent } from './itinerario/itinerario.component'

const routes: Routes = [
  { path: 'lista-lotacao', component: ListaLotacaoComponent },
  { path: 'lista-onibus', component: ListaOnibusComponent },
  { path: 'itinerario/:id', component: ItinerarioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
