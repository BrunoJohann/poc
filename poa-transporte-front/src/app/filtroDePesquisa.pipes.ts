import { Pipe, PipeTransform } from '@angular/core';


@Pipe({name: 'filtroGenerico'})
export class FiltroDePesquisa implements PipeTransform {
  transform(value: any[], term: string): any[] {
    return term ? value.filter((x) => 
              x.nome.toLowerCase().includes( term.toLowerCase()) 
              || x.codigo.toLowerCase().includes(term.toLowerCase())
              || x.id.toLowerCase().includes(term.toLocaleLowerCase()))
            : value
  } 
}