import { Component, OnInit } from '@angular/core';
import { ListaLotacaoService } from '../services/listaLotacao/lista-lotacao.service';

@Component({
  selector: 'app-lista-onibus',
  templateUrl: './lista-onibus.component.html',
  styleUrls: ['./lista-onibus.component.css']
})
export class ListaOnibusComponent implements OnInit {

  private searchText: string = '';
  private listaOnibus: Array<any>;

  constructor( private listaService: ListaLotacaoService ) { }

  ngOnInit() {
    this.listar('l');
  }

  listar(tipo) {
      this.listaService.listar(tipo).subscribe( dados => {
        this.listaOnibus = dados
      } 
    );
  }

}
