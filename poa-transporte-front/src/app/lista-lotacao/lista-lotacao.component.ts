import { Component, OnInit } from '@angular/core';
import { ListaLotacaoService } from '../services/listaLotacao/lista-lotacao.service';

@Component({
  selector: 'app-lista-lotacao',
  templateUrl: './lista-lotacao.component.html',
  styleUrls: ['./lista-lotacao.component.css']
})
export class ListaLotacaoComponent implements OnInit {

  private searchText: string = '';
  private listaLotacao: Array<any>;

  constructor( private listaService: ListaLotacaoService ) { }

  ngOnInit() {
    this.listar('o');
  }

  listar(tipo) {
      this.listaService.listar(tipo).subscribe( dados => {
        this.listaLotacao = dados
      } 
    );
  }

}
