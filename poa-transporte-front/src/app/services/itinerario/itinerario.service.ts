import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItinerarioService {

  url(id) {
    return `http://www.poatransporte.com.br/php/facades/process.php?a=il&p=${id}`
  }

  constructor( private http: HttpClient ) { }

  listar(id) {
    return this.http.get<any>(this.url(id))
  }

}
