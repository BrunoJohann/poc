import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListaLotacaoService {

  Url(tipo) {
    return `http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%25&t=${tipo}`
  }

  constructor( private http: HttpClient ) { }

  listar(tipo) {
    return this.http.get<any>(`${this.Url(tipo)}`)
  }
}
