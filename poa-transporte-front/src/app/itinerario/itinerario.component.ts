import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItinerarioService } from '../services/itinerario/itinerario.service';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {

  private itinerario: Array<Object> = new Array();
  private nomeDoItinerario: string;
  private codigo: string;

  constructor( private route: ActivatedRoute,
               private itinerarioService: ItinerarioService ) { }

  ngOnInit() {
    this.route.params.subscribe( res => { 
      this.listar(res.id)
     })
  }

  listar(id) {
    this.itinerarioService.listar(id).subscribe( dados => {
      let ponto = 0
      this.nomeDoItinerario = dados.nome
      this.codigo = dados.codigo
        for(let parada in dados) {
          let lat = dados[parada].lat
          let lng =  dados[parada].lng
          this.itinerario.push([lat, lng, ponto])
          ponto++
        }
        for(let i = 1; i <= 3; i++) {
          this.itinerario.pop();
        }    
      } 
    )
  }
}
