import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ListaOnibusComponent } from './lista-onibus/lista-onibus.component';

import { ListaLotacaoComponent } from './lista-lotacao/lista-lotacao.component';
import { ListaLotacaoService } from './services/listaLotacao/lista-lotacao.service';

import { ItinerarioComponent } from './itinerario/itinerario.component'
import { ItinerarioService } from './services/itinerario/itinerario.service';

import { FormsModule } from '@angular/forms';
import { FiltroDePesquisa } from './filtroDePesquisa.pipes';

@NgModule({
  declarations: [
    AppComponent,
    ListaOnibusComponent,
    ListaLotacaoComponent,
    ItinerarioComponent,
    FiltroDePesquisa
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ 
    ListaLotacaoService, 
    ItinerarioService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
